pub enum GetResponse {
    Redirect(String),
    // Ok(&'a dyn std::io::Read)
}

pub enum PutResponse {
    Ok,
    Redirect(String),
}
