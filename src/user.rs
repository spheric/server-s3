use anyhow::Result;
use serde::Serialize;
use uuid::Uuid;

#[derive(Serialize, Clone)]
pub struct User {
    pub id: Uuid,
}

impl User {
    pub fn from_uuid(user_id: Uuid) -> Self {
        User { id: user_id }
    }

    pub fn from_uuid_str(user_id: &str) -> Result<Self> {
        let user_id = Uuid::parse_str(user_id)?;
        Ok(User { id: user_id })
    }
}
