use crate::AppState;
use anyhow::{bail, Result};
use axum::{
    extract::State,
    http::{Request, StatusCode},
    middleware::Next,
    response::Response,
};
use axum_extra::extract::CookieJar;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use crate::user::User;
use hmac::Hmac;
use jwt::VerifyWithKey;
use sha2::Sha256;
use std::collections::BTreeMap;

pub async fn auth<B>(
    State(state): State<AppState>,
    jar: CookieJar,
    mut req: Request<B>,
    next: Next<B>,
) -> Result<Response, StatusCode> {
    if let Some(auth_token) = jar.get("auth_token") {
        match authorize_current_user(state.jwt_key, auth_token.value()).await {
            Ok(user) => {
                req.extensions_mut().insert(user);
                Ok(next.run(req).await)
            }
            Err(e) => {
                println!("The auth error was: {}", e);
                Err(StatusCode::UNAUTHORIZED)
            }
        }
    } else {
        println!("No token found");
        Err(StatusCode::UNAUTHORIZED)
    }
}

async fn authorize_current_user(key: Hmac<Sha256>,auth_token: &str) -> Result<User> {
    let claims: BTreeMap<String, String> = auth_token.verify_with_key(&key)?;

    let now = SystemTime::now().duration_since(UNIX_EPOCH)?;

    if let Some(nbf) = claims.get("nbf") {
        let nbf: u64 = nbf.parse()?;
        let nbf = Duration::new(nbf, 0);
        if now < nbf {
            bail!("violating nbf")
        }
    } else {
        bail!("missing nbf claim")
    }

    if let Some(exp) = claims.get("exp") {
        let exp: u64 = exp.parse()?;
        let exp = Duration::new(exp, 0);
        if now > exp {
            bail!("violating exp")
        }
    } else {
        bail!("missing exp claim")
    }

    if let Some(user_id) = claims.get("sub") {
        User::from_uuid_str(user_id)
    } else {
        bail!("missing sub claim")
    }
}
