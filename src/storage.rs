use crate::backend::{GetResponse, PutResponse};
use anyhow::Result;
use http::{
    header::{HeaderName, HeaderValue},
    HeaderMap,
};
use std::collections::HashMap;

#[derive(Clone)]
pub struct Storage {
    pub bucket: s3::Bucket,
    signature_expiration: u32,
}

const CONTENT_MD5: HeaderName = HeaderName::from_static("content-md5");

impl Storage {
    pub fn new(bucket: s3::Bucket, signature_expiration: u32) -> Self {
        Self {
            bucket,
            signature_expiration,
        }
    }

    pub fn get(&self, name: &str, if_none_match: Option<&HeaderValue>) -> Result<GetResponse> {
        let request_parameters = match if_none_match {
            Some(if_none_match) => Some(HashMap::from([(
                String::from("If-None-Match"),
                String::from(if_none_match.to_str()?),
            )])),
            None => None,
        };

        let presigned_url =
            self.bucket
                .presign_get(name, self.signature_expiration, request_parameters)?;

        Ok(GetResponse::Redirect(presigned_url))
    }

    pub fn put(
        self,
        name: &str,
        content_md5: HeaderValue,
        if_match: Option<&HeaderValue>,
        if_none_match: Option<&HeaderValue>,
    ) -> Result<PutResponse> {
        let mut custom_headers = HeaderMap::with_capacity(2);
        custom_headers.insert(CONTENT_MD5, content_md5.clone());

        if let Some(if_match) = if_match {
            custom_headers.insert(http::header::IF_MATCH, if_match.clone());
        }
        if let Some(if_none_match) = if_none_match {
            custom_headers.insert(http::header::IF_NONE_MATCH, if_none_match.clone());
        }

        let presigned_url =
            self.bucket
                .presign_put(name, self.signature_expiration, Some(custom_headers))?;

        Ok(PutResponse::Redirect(presigned_url))
    }
}
