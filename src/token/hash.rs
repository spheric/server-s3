use anyhow::Result;
use argon2::{
    password_hash::{PasswordHash, PasswordVerifier},
    Argon2,
};

pub fn verify(password: &[u8], hash: &str) -> Result<()> {
    let hash = PasswordHash::new(hash)?;
    Argon2::default().verify_password(password, &hash)?;

    Ok(())
}
