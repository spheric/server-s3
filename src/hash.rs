use argon2::{
    password_hash::{rand_core::OsRng, PasswordHasher, SaltString},
    Argon2,
};
use sha2::{Digest, Sha512};

pub async fn hash(password: String) -> String {
    let mut hasher = Sha512::new();
    hasher.update(password.as_bytes());
    let password = hasher.finalize();

    let salt = SaltString::generate(&mut OsRng);

    // Argon2 with default params (Argon2id v19)
    let argon2 = Argon2::default();

    let hash = argon2.hash_password(&password, &salt).unwrap();

    hash.to_string()
}
