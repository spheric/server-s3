use anyhow::Result;
use axum::{
    extract,
    http::{header, HeaderMap, StatusCode},
    middleware,
    response::{IntoResponse, Response},
    routing::{get, post, put},
    Extension, Json, Router,
};
use hmac::{Hmac, Mac};
use s3::{bucket::Bucket, creds::Credentials, Region};
use serde::Deserialize;
use sha2::Sha256;
use std::env;
use std::net::SocketAddr;
use uuid::Uuid;

mod auth;
mod backend;
mod hash;
mod storage;
mod token;
mod user;
use user::User;

#[derive(Clone)]
pub struct AppState {
    pub storage: storage::Storage,
    pub jwt_key: Hmac<Sha256>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let storage = {
        let region = env::var("AWS_REGION")?;
        let s3_bucket = env::var("AWS_S3_BUCKET")?;
        let s3_access_key = env::var("AWS_ACCESS_KEY_ID")?;
        let s3_secret_key = env::var("AWS_SECRET_ACCESS_KEY")?;
        let cloudflare_account_id = match env::var("CLOUDFLARE_ACCOUNT_ID") {
            Err(_) => None,
            Ok(id) => Some(id),
        };

        let region = match cloudflare_account_id {
            None => region.parse().unwrap(),
            Some(id) => Region::R2 { account_id: id },
        };

        let bucket = Bucket::new(
            &s3_bucket,
            region,
            Credentials::new(Some(&s3_access_key), Some(&s3_secret_key), None, None, None).unwrap(),
        )?
        .with_path_style();

        storage::Storage::new(bucket, 60)
    };

    let shared_state = {
        // TODO: use asymmetric encryption for signing and share the public key with the other
        // server instances
        let jwt_key: Hmac<Sha256> = Hmac::new_from_slice(&rand::random::<[u8; 32]>())?;
        AppState { storage, jwt_key }
    };

    let object_router = Router::new()
        .route("/v1/:namespace/:key", get(object_get))
        .route("/v1/:namespace/:key", put(object_put))
        .route_layer(middleware::from_fn_with_state(
            shared_state.clone(),
            auth::auth,
        ));

    let app = Router::new()
        .route("/", get(root))
        .route("/hash", post(hash::hash))
        .nest("/objects", object_router)
        .route("/token/v1", get(token::handler))
        .route("/signup/v1", post(signup))
        .with_state(shared_state);

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}

async fn root() -> &'static str {
    "Spheric server"
}

#[derive(Deserialize)]
struct ObjectParams {
    namespace: Uuid,
    key: Uuid,
}

async fn object_get(
    extract::Path(ObjectParams { namespace, key }): extract::Path<ObjectParams>,
    extract::State(state): extract::State<AppState>,
    headers: HeaderMap,
    Extension(current_user): Extension<User>,
) -> Response {
    if namespace != current_user.id {
        return (StatusCode::FORBIDDEN).into_response();
    };
    let get_result = state.storage.get(
        &format!("objects/v1/{}/{}", namespace, &key),
        headers.get(http::header::IF_NONE_MATCH),
    );

    let get_result = match get_result {
        Ok(res) => res,
        Err(_) => return (StatusCode::INTERNAL_SERVER_ERROR).into_response(),
    };

    match get_result {
        backend::GetResponse::Redirect(location) => (
            StatusCode::TEMPORARY_REDIRECT,
            [(header::LOCATION, location)],
        )
            .into_response(),
    }
}

async fn object_put(
    extract::Path(ObjectParams { namespace, key }): extract::Path<ObjectParams>,
    extract::State(state): extract::State<AppState>,
    headers: HeaderMap,
    Extension(current_user): Extension<User>,
) -> Response {
    if namespace != current_user.id {
        return (StatusCode::FORBIDDEN).into_response();
    };

    let content_md5 = match headers.get("Content-MD5") {
        Some(h) => h,
        None => {
            return (
                StatusCode::BAD_REQUEST,
                "{\"error_message\": \"The request is missing a content-md5 header.\"}",
            )
                .into_response();
        }
    };

    let if_match = headers.get(http::header::IF_MATCH);
    let if_none_match = headers.get(http::header::IF_NONE_MATCH);

    if if_match.is_some() == if_none_match.is_some() {
        return (
            StatusCode::BAD_REQUEST,
            "{\"error_message\": \"One and only one of if-match and if-none-match must be set.\"}",
        )
            .into_response();
    }

    let put_result = state.storage.put(
        &format!("objects/v1/{}/{}", namespace, &key),
        content_md5.clone(),
        if_match,
        if_none_match,
    );

    let put_result = match put_result {
        Ok(u) => u,
        Err(_) => {
            return (StatusCode::BAD_REQUEST).into_response();
        }
    };

    match put_result {
        backend::PutResponse::Ok => (StatusCode::OK).into_response(),
        backend::PutResponse::Redirect(location) => (
            StatusCode::TEMPORARY_REDIRECT,
            [(header::LOCATION, location)],
        )
            .into_response(),
    }
}

#[derive(Deserialize)]
struct CreateUser {
    password: String,
    code: String,
}

const EMPTY_STRING_MD5 :&str= "d41d8cd98f00b204e9800998ecf8427e";

// signup stores a hash of the new user's password, if the registration code is valid an unused.
// Each code is an object under `/signup/codes/`. A code is valid if the object is empty.
// To mark the code as used, the new user_id is stored in the code file.
async fn signup(
    extract::State(state): extract::State<AppState>,
    extract::Json(create_user): extract::Json<CreateUser>,
) -> Response {
    let user_id = Uuid::new_v4();
    let mut headers =  HeaderMap::with_capacity(1);
        headers.insert(http::header::IF_MATCH, EMPTY_STRING_MD5.parse().unwrap());
    let bucket = state.storage.bucket.with_extra_headers(headers);
    if let Ok(response) = bucket
        .put_object(
            format!("signup/codes/{}", create_user.code),
            user_id.to_string().as_bytes(),
        )
        .await
    {
        if response.status_code() == 200
            && response.headers().get("content-length") == Some(&"0".to_string())
        {
            let password_hash = hash::hash(create_user.password).await;
            if let Err(e) = state
                .storage
                .bucket
                .put_object(
                    format!("auth/v1/{}/passwd-hash", user_id),
                    password_hash.as_bytes(),
                )
                .await
            {
                return (StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response();
            }
            let u = user::User::from_uuid(user_id);
            return (Json(u)).into_response();
        };
    };
    (StatusCode::FORBIDDEN).into_response()
}
