use crate::AppState;
use crate::User;
use anyhow::anyhow;
use anyhow::Result;
use axum::{
    extract::{State, TypedHeader},
    headers::authorization::{Authorization, Bearer},
    http::StatusCode,
};
use axum_extra::extract::{cookie::Cookie, CookieJar};
use base64::{engine::general_purpose, Engine as _};
use hmac::Hmac;
use jwt::SignWithKey;
use sha2::Sha256;
use std::collections::BTreeMap;
use std::time::{SystemTime, UNIX_EPOCH};

use crate::storage::Storage;

mod hash;

pub async fn handler(
    State(state): State<AppState>,
    TypedHeader(auth): TypedHeader<Authorization<Bearer>>,
    jar: CookieJar,
) -> Result<CookieJar, StatusCode> {
    match authenticate(state.storage, auth.token()).await {
        Ok(current_user) => {
            let jwt_token = jwt(state.jwt_key, current_user, 300).unwrap();
            Ok(jar.add(
                Cookie::build("auth_token", jwt_token)
                    .path("/")
                    .secure(true)
                    .http_only(true)
                    .finish(),
            ))
        }
        Err(e) => {
            println!("The auth error was: {}", e);
            Err(StatusCode::UNAUTHORIZED)
        }
    }
}

async fn authenticate(storage: Storage, auth_token: &str) -> Result<User> {
    let mut split = auth_token.splitn(3, ':');
    let version = split.next().ok_or(anyhow!("No version identifier"))?;
    if version != "v1" {
        return Err(anyhow!("Unknown auth version {}", version));
    }
    let user_id = split.next().ok_or(anyhow!("No user ID"))?;

    let password = {
        let encoded = split.next().ok_or(anyhow!("No password"))?;
        general_purpose::STANDARD_NO_PAD.decode(encoded)?
    };

    let response = storage
        .bucket
        .get_object(format!("auth/v1/{}/passwd-hash", user_id))
        .await?;
    let hash = response.as_str()?;
    match hash::verify(&password, hash) {
        Ok(_) => User::from_uuid_str(user_id),
        Err(e) => Err(e),
    }
}

fn jwt(key: Hmac<Sha256>, user: User, ttl: u64) -> Result<String> {
    let now = SystemTime::now().duration_since(UNIX_EPOCH)?;

    let mut claims = BTreeMap::new();
    claims.insert("sub", user.id.to_string());
    claims.insert("nbf", format!("{}", now.as_secs() - 5));
    claims.insert("exp", format!("{}", now.as_secs() + ttl));

    Ok(claims.sign_with_key(&key)?.to_string())
}
