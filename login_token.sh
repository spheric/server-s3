#!/usr/bin/env bash
set -Eeuo pipefail

# Login with:
# ```
# curl \
#     -sS \
#     -c cookiejar \
#     -H "Authorization: Bearer $(./login_token.sh "6dd909fb-fe5d-4e15-bb00-71791eb858cf" "password")" \
#     'http://localhost:3000/token/v1'
# ```


declare -r \
	user_id="$1" \
	password="$2"

printf 'v1:%s:%s' "$user_id" "$(printf '%s' "$password" | sha512sum | xxd -r -p | base64 -w0 | tr -d '=')"
