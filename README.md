# spheric server-s3

This `spheric-server` implementation uses an S3-compatible object store as its backend.
This server uses request signing to allow for direct upload and download from the client to the object storage.

## Implementation details

This server stores in the S3-compatible backend:
* user objects under `/objects/v1/${user_identifier}/${object_identifier}`
* user password hashes under `/auth/v1/${user_identifier}/passwd-hash`
* user public keys under `/pubkeys/v1/${user_identifier}/share-key`
* user access control lists under `/share/v1/${user_identifier}/${object_identifier}`
* registration codes under `/signup/codes/codes/${code}`

### Example calls

The curl calls here represent the calls sent by the frontend.

First, save the authentication cookie in a cookie jar:

```shell
curl \
    -sS \
    -c cookiejar \
    -H 'Authorization: Bearer v1:6ffe201e-8a7c-4260-a4c1-556d98ab6c4d:an9CKnk47mZBM2VbxkANP8e7IYlRWQ5jSmpgl5x0mI9bvFvQSpLSaTO6OVkJjeSWge+QGKRZW0z2VviFMfAttg' \
    'http://localhost:3000/token/v1'
```

File creation uses `If-None-Match: *` to reject in case the file exists:

```shell
curl \
    -isSL \
    -X PUT \
    -b cookiejar \
    -H 'If-None-Match: *' \
    -H "Content-MD5: $(openssl dgst -md5 -binary < README.md | base64)" \
    --data-binary @README.md \
    'http://localhost:3000/objects/v1/6ffe201e-8a7c-4260-a4c1-556d98ab6c4d/761b220c-b7c6-4d6a-b8d4-2f7719d18bd6'
```

File update uses `If-Match` to reject in case the file has changed its content after the last read from this client:

```shell
curl \
    -isSL \
    -X PUT \
    -b cookiejar \
    -H 'If-Match: "c6544773b3e72fefd5770da3ed50f67b"' \
    -H "Content-MD5: $(openssl dgst -md5 -binary < README.md | base64)" \
    --data-binary @README.md \
    'http://localhost:3000/objects/v1/6ffe201e-8a7c-4260-a4c1-556d98ab6c4d/761b220c-b7c6-4d6a-b8d4-2f7719d18bd6'
```

To GET a file:

```shell
curl \
    -isSL \
    -X GET \
    -b cookiejar \
    'http://localhost:3000/objects/v1/6ffe201e-8a7c-4260-a4c1-556d98ab6c4d/761b220c-b7c6-4d6a-b8d4-2f7719d18bd6'
```

To refresh the local copy of a file avoiding unnecessary transfers, provide its ETag with `If-None-Match`:

```shell
curl \
    -isSL \
    -X GET \
    -b cookiejar \
    -H 'If-None-Match: "c6544773b3e72fefd5770da3ed50f67b"' \
    'http://localhost:3000/objects/v1/6ffe201e-8a7c-4260-a4c1-556d98ab6c4d/761b220c-b7c6-4d6a-b8d4-2f7719d18bd6'
```

Once you get a `401 Unauthorized` error, get a new token.
